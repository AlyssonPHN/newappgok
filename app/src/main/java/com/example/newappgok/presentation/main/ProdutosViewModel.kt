package com.example.newappgok.presentation.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.newappgok.R
import com.example.newappgok.data.model.Produtos
import com.example.newappgok.data.repository.ProdutosRepository
import com.example.newappgok.data.results.ProdutosResult

class ProdutosViewModel(val dataSource: ProdutosRepository) : ViewModel() {
    val produtosLiveData = MutableLiveData<Produtos>()
    val viewFlipperLiveData: MutableLiveData<Pair<Int, Int?>> = MutableLiveData()

    fun getProdutos(){
        dataSource.getProdutos {result: ProdutosResult ->  
            when(result){
                is ProdutosResult.ShowProgress -> {
                    viewFlipperLiveData.value = Pair(VIEW_PROGRESS, null)
                }

                is ProdutosResult.Succes -> {
                    produtosLiveData.value = result.produtos
                    viewFlipperLiveData.value = Pair(VIEW_PRODUTOS, null)
                }
                is ProdutosResult.ApiError -> {
                    if (result.statusCode == 401) {
                        //viewLiveData.value = Pair(VIEW_ERROR, R.string.error_401)
                        viewFlipperLiveData.value = Pair(VIEW_ERROR, R.string.error_401)
                    } else {
                        viewFlipperLiveData.value = Pair(VIEW_ERROR, R.string.error_nao_tratado)
                    }
                }
                is ProdutosResult.ServerError -> {
                    viewFlipperLiveData.value = Pair(VIEW_ERROR, R.string.error_500)
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class ViewModelFactory(private val dataSource: ProdutosRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ProdutosViewModel::class.java)) {
                return ProdutosViewModel(dataSource) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    companion object {
        const val VIEW_PROGRESS = 0
        const val VIEW_PRODUTOS = 1
        const val VIEW_ERROR = 2
    }
}