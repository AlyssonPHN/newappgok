package com.example.newappgok.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newappgok.R
import com.example.newappgok.data.repository.ProdutosApiDataSource
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tela_erro.*
import kotlinx.android.synthetic.main.tela_produtos_principal.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel: ProdutosViewModel =
            ProdutosViewModel.ViewModelFactory(ProdutosApiDataSource())
                .create(ProdutosViewModel::class.java)

        viewModel.produtosLiveData.observe(this, {
            it?.let { produtos ->
                // Spotlight
                with(viewPager) {
                    offscreenPageLimit = 1
                    // increase this offset to show more of left/right
                    val offsetPx = 50
                    this.setPadding(offsetPx, 0, offsetPx, 0)

                    // Set Adapter
                    adapter = SpotlightAdapter(produtos.spotlight)
                }

                // Cash Banner
                with(imgviewCash) {
                    setImageResource(R.drawable.ic_launcher_background)
                    Glide.with(this@MainActivity).load(produtos.cash.bannerURL)
                        .fitCenter()
                        .centerCrop()
                        .placeholder(R.drawable.ic_image_broken)
                        .into(this)
                    this.contentDescription = produtos.cash.description
                }

                // Products List
                with(recyclerViewProducts) {
                    layoutManager =
                        LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL, false)
                    adapter = ProductsAdapter(produtos.products)
                }
            }
        })

        viewModel.viewFlipperLiveData.observe(this, {
            it?.let { viewFlipper ->

                viewFlipperProdutos.displayedChild = viewFlipper.first

                viewFlipper.second?.let { messageId ->
                    erroMessage.text = getString(messageId)
                }
            }
        })

        btnAtualizar.setOnClickListener {
            viewModel.getProdutos()
        }

        viewModel.getProdutos()
    }
}