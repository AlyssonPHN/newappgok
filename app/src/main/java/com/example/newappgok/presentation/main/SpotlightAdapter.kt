package com.example.newappgok.presentation.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.newappgok.R
import com.example.newappgok.data.model.Spotlight
import kotlinx.android.synthetic.main.list_spotlight.view.*


class SpotlightAdapter(
    private val spotlightList: List<Spotlight>
) : RecyclerView.Adapter<SpotlightAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_spotlight, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val spotlight = spotlightList[position]
        holder.bind(spotlight)

    }

    override fun getItemCount(): Int = spotlightList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageview = view.imageView

        fun bind(spotlight: Spotlight){
            Glide.with(imageview.context).load(spotlight.bannerURL)
                .transition(DrawableTransitionOptions.withCrossFade())
                .centerCrop()
                .placeholder(R.drawable.ic_image_broken)
                .into(imageview)
            imageview.contentDescription = spotlight.description
        }
    }

}