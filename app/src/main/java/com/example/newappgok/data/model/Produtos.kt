package com.example.newappgok.data.model

data class Produtos (
    val spotlight: List<Spotlight>,
    val products: List<Product>,
    val cash: Cash
)