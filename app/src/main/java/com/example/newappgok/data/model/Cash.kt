package com.example.newappgok.data.model

data class Cash (
    val title: String,
    val bannerURL: String,
    val description: String
)