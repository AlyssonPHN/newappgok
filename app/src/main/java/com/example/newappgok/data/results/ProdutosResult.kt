package com.example.newappgok.data.results

import com.example.newappgok.data.model.Produtos

sealed class ProdutosResult {
    object ShowProgress : ProdutosResult()
    class Succes(val produtos: Produtos): ProdutosResult()
    class ApiError(val statusCode: Int) : ProdutosResult()
    object ServerError : ProdutosResult()
}