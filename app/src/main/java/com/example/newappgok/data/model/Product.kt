package com.example.newappgok.data.model

data class Product (
    val name: String,
    val imageURL: String,
    val description: String
)