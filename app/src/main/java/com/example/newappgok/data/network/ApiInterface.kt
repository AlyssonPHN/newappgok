package com.example.newappgok.data.network

import com.example.newappgok.data.model.Produtos
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("products")
    fun getProdutos(): Call<Produtos>
}