package com.example.newappgok.data.repository

import com.example.newappgok.data.model.Produtos
import com.example.newappgok.data.network.ApiClient
import com.example.newappgok.data.results.ProdutosResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProdutosApiDataSource: ProdutosRepository {

    override fun getProdutos(produtosResultCallback: (result: ProdutosResult) -> Unit) {
        produtosResultCallback(ProdutosResult.ShowProgress)

        ApiClient.service.getProdutos().enqueue(object : Callback<Produtos>{
            override fun onResponse(call: Call<Produtos>, response: Response<Produtos>) {
                when {
                    response.isSuccessful -> {
                        produtosResultCallback(ProdutosResult.Succes(response.body()!!))
                    }
                    else -> {
                        produtosResultCallback(ProdutosResult.ApiError(response.code()))
                    }
                }
            }

            override fun onFailure(call: Call<Produtos>, t: Throwable) {
                produtosResultCallback(ProdutosResult.ServerError)
            }

        })

    }

}