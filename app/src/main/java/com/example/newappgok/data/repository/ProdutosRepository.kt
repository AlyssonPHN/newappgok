package com.example.newappgok.data.repository

import com.example.newappgok.data.results.ProdutosResult

interface ProdutosRepository {
    fun getProdutos(produtosResultCallback: (result: ProdutosResult) -> Unit)
}