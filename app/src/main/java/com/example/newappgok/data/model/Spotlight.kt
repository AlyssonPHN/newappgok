package com.example.newappgok.data.model

data class Spotlight (
    val name: String,
    val bannerURL: String,
    val description: String
)