package com.example.newappgok.presentation.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.newappgok.R
import com.example.newappgok.data.model.Cash
import com.example.newappgok.data.model.Produtos
import com.example.newappgok.data.repository.ProdutosRepository
import com.example.newappgok.data.results.ProdutosResult
import com.example.newappgok.presentation.main.ProdutosViewModel.Companion.VIEW_ERROR
import com.example.newappgok.presentation.main.ProdutosViewModel.Companion.VIEW_PRODUTOS
import com.example.newappgok.presentation.main.ProdutosViewModel.Companion.VIEW_PROGRESS
import com.nhaarman.mockitokotlin2.verify
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ProdutosViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var produtosLiveDataObserver: Observer<Produtos>

    @Mock
    private lateinit var viewFlipperLiveDataObserver: Observer<Pair<Int, Int?>>

    private lateinit var viewModel: ProdutosViewModel

    @Test
    fun `when viewmodel getProdutos start Progress`(){
        //Arrange
        val resultShowProgress = MockRepository(ProdutosResult.ShowProgress)
        viewModel = ProdutosViewModel(resultShowProgress)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperLiveDataObserver)
        //Act
        viewModel.getProdutos()

        //Assert
        verify(viewFlipperLiveDataObserver).onChanged(Pair(VIEW_PROGRESS, null))
    }

    @Test
    fun `when viewmodel getProdutos get sucess then sets produtosLiveData`(){
        // Arrange
        val produtos = Produtos(spotlight = listOf(), products = listOf(), cash = Cash("","",""))
        val resultSucess = MockRepository(ProdutosResult.Succes(produtos))
        viewModel = ProdutosViewModel(resultSucess)
        viewModel.produtosLiveData.observeForever(produtosLiveDataObserver)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperLiveDataObserver)

        // Act
        viewModel.getProdutos()

        // Assert
        verify(produtosLiveDataObserver).onChanged(produtos)
        verify(viewFlipperLiveDataObserver).onChanged(Pair(VIEW_PRODUTOS, null))
    }

    @Test
    fun `when viewmodel getProdutos get server error then sets produtosLiveData`(){
        // Arrange
        val resultServerError = MockRepository(ProdutosResult.ServerError)
        viewModel = ProdutosViewModel(resultServerError)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperLiveDataObserver)

        //Act
        viewModel.getProdutos()

        //Assert
         verify(viewFlipperLiveDataObserver).onChanged(Pair(VIEW_ERROR, R.string.error_500))

    }

    @Test
    fun `when viewmodel getProdutos get error code 401 then sets produtosLiveData`(){
        // Arrange
        val resultApiError = MockRepository(ProdutosResult.ApiError(401))
        viewModel = ProdutosViewModel(resultApiError)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperLiveDataObserver)

        //Act
        viewModel.getProdutos()

        //Assert
        verify(viewFlipperLiveDataObserver).onChanged(Pair(VIEW_ERROR, R.string.error_401))
    }

    @Test
    fun `when viewmodel getProdutos get error code different 401 then sets produtosLiveData`(){
        // Arrange
        val resultApiError = MockRepository(ProdutosResult.ApiError(40))
        viewModel = ProdutosViewModel(resultApiError)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperLiveDataObserver)

        //Act
        viewModel.getProdutos()

        //Assert
        verify(viewFlipperLiveDataObserver).onChanged(Pair(VIEW_ERROR, R.string.error_nao_tratado))
    }

    class MockRepository(private val result: ProdutosResult) : ProdutosRepository {
        override fun getProdutos(produtosResultCallback: (result: ProdutosResult) -> Unit) {
            produtosResultCallback(result)
        }

    }

}